.. _thermal-qa-tests:

Thermal QA Tests
================

Steady Thermal Conduction
-------------------------
* :ref:`thermal-steady-1D-conduction-BC-1st-kind`

* :ref:`thermal-steady-1D-conduction-BC-1st-2nd-kind`

* :ref:`thermal-steady-2D-conduction-BC-1st-kind`

* :ref:`thermal-steady-2D-conduction-BC-1st-2nd-kind`

* :ref:`thermal-steady-3D-conduction-BC-1st-kind`


Transient Thermal Conduction
----------------------------
* :ref:`thermal-transient-1D-conduction-BC-1st-kind`

* :ref:`thermal-transient-1D-conduction-BC-2nd-kind`

* :ref:`thermal-transient-1D-conduction-BC-1st-2nd-kind`

* :ref:`thermal-transient-2D-conduction-BC-1st-2nd-kind`

