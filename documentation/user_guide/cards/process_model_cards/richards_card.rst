Back to :ref:`card-index`

Back to :ref:`subsurface-flow-card`

Back to :ref:`mode-card`

.. _richards-card:

RICHARDS
========

Defines options for the Richards subsurface flow mode.

Options
-------

There are currently no options.
 
Examples
--------
::

 ...
 PROCESS_MODELS
   SUBSURFACE_FLOW flow
     MODE RICHARDS
   /
 /
 ...
