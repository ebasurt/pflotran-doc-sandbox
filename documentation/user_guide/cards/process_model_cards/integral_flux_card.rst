Back to :ref:`card-index`

.. _integral-flux-card:

INTEGRAL_FLUX
=============
Sets up a surface through which fluxes of all primary dependent variables can 
be calculated. 

**Note: Be sure to add the PERIODIC_OBSERVATION card to the** 
:ref:`output-card` **card as this toggles on the printing of integral fluxes**
**to a file with the suffix '?-int.dat'.**

Required Cards:
---------------------
INTEGRAL_FLUX <optional string>
 Opens the INTERGAL_FLUX block and provides the name.  
 The name is optional, but if not provided, the name must be specified through 
 the optional NAME card below.

COORDINATES
 Opens a COORDINATES block listing coordinates in x,y,z that define the 
 rectangle over which the flux will be calculated. The coordinates must form a 
 rectangular plane that is aligned with the coordinate axes.

Optional Cards:
--------------------
INVERT_DIRECTION
 Inverts the sign of the flux. For fluxes at upwind boundaries, influx will be 
 negative. This has no impact on the actual flux values other than to flip the 
 sign.

NAME <string>
 Specifies a name that is associated with the integral fluxes in the "?-int.dat" 
 file.  This name will overwrite any name specified with the INTEGRAL_FLUX card 
 that opens the block.


Examples
--------
 ::

  INTEGRAL_FLUX flux_up_shaft
    COORDINATES
      25.d0 15.d0 300.d0
      35.d0 20.d0 300.d0
    /
  /

  INTEGRAL_FLUX
    NAME inflow
    COORDINATES
      0.d0 0.d0 0.d0
      0.d0 10.d0 5.d0
    /
    INVERT_DIRECTION
  /
