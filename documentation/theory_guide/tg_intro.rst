Introduction to the Governing Equations
=======================================

* :ref:`symbol-glossary`

* :ref:`mode-richards`

* :ref:`mode-th`

* :ref:`mode-general`

* :ref:`mode-mphase`

* :ref:`mode-immis`

* :ref:`mode-miscible`

* :ref:`mode-reactive-transport`

* :ref:`mode-geomechanics`

* :ref:`multiple_continuum`