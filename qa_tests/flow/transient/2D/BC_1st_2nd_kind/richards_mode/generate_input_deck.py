import sys

nx = 1; dx = 1; lx = 1
ny = 1; dy = 1; ly = 1
nz = 1; dz = 1; lz = 1
filename = ''
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])
  if sys.argv[k] == '-input_prefix':
    filename = sys.argv[k+1] + '.in'
    
f = open(filename,'w')

simulation = """ 
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE RICHARDS
    /
  /
END

"""
grid = """
GRID
  TYPE structured
  NXYZ """ + str(nx) + " " + str(ny) + " " + str(nz) + """
  DXYZ
   """ + str(dx) + """d0
   """ + str(dy) + """d0
   """ + str(dz) + """d0
  END
END
"""
region = """
REGION all
  COORDINATES
    0.0d0 0.0d0 0.0d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION west_face
  FACE WEST
  COORDINATES
    0.d0 0.d0 0.d0
    0.d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION east_face
  FACE EAST
  COORDINATES
    """ + str(lx) + """d0 0.d0 0.d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION north_face
  FACE NORTH
  COORDINATES
    0.d0 """ + str(ly) + """d0 0.d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION south_face
  FACE SOUTH
  COORDINATES
    0.d0  0.d0  0.d0
    """ + str(lx) + "d0 0.d0 " + str(lz) + """d0
  /
END
"""
mat_prop = """
MATERIAL_PROPERTY plate
  ID 1
  POROSITY 0.20
  TORTUOSITY 1.d0
  ROCK_DENSITY 2.0E3
  CHARACTERISTIC_CURVES cc1
  PERMEABILITY
    PERM_X 1.d-14
    PERM_Y 1.d-14
    PERM_Z 1.d-14
  /
END
"""
char_curves = """
CHARACTERISTIC_CURVES cc1
  SATURATION_FUNCTION VAN_GENUCHTEN
    LIQUID_RESIDUAL_SATURATION 0.5d-1
    M 0.75
    ALPHA 1.d-3
  /
  PERMEABILITY_FUNCTION MUALEM
    LIQUID_RESIDUAL_SATURATION 0.1d0
    M 0.5d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_GAS
    LIQUID_RESIDUAL_SATURATION 0.1d0
    GAS_RESIDUAL_SATURATION 0.1d0
    M 0.5d0
  /
END
"""
eoswater = """
EOS WATER
  DENSITY EXPONENTIAL 1000. 101325. 1.0d-9
  VISCOSITY CONSTANT 1.728d-3 Pa-s
END
"""
fluid_prop = """
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END
"""
strata = """
STRATA
  REGION all
  MATERIAL plate
END
"""
time = """
TIME
  FINAL_TIME 0.10 day
  INITIAL_TIMESTEP_SIZE 1.d-4 day
  MAXIMUM_TIMESTEP_SIZE 0.0001 day at 0.d0 day
END
"""
output = """
OUTPUT
  SNAPSHOT_FILE 
    #NO_PRINT_INITIAL
    TIMES day 0.01 0.04 0.06
    #NO_PRINT_FINAL
    FORMAT HDF5
  /
END
"""
dataset = """
DATASET pressure_initial
  HDF5_DATASET_NAME initial
  FILENAME ../dataset.h5
END
"""
flow_cond = """
FLOW_CONDITION initial
  TYPE
    PRESSURE dirichlet
  /
  PRESSURE DATASET pressure_initial
END

FLOW_CONDITION west_face
  TYPE
    PRESSURE dirichlet
  /
  PRESSURE 101325. Pa
END

FLOW_CONDITION east_face
  TYPE
    PRESSURE dirichlet
  /
  PRESSURE 101325. Pa
END

FLOW_CONDITION north_face
  TYPE
    FLUX neumann
  /
  FLUX 0.d0 m/yr
END

FLOW_CONDITION south_face
  TYPE
    FLUX neumann
  /
  FLUX 0.d0 m/yr
END
"""
init_cond = """
INITIAL_CONDITION initial
  REGION all
  FLOW_CONDITION initial
END

BOUNDARY_CONDITION west_face
  REGION west_face
  FLOW_CONDITION west_face
END

BOUNDARY_CONDITION east_face
  FLOW_CONDITION east_face
  REGION east_face
END

BOUNDARY_CONDITION north_face
  REGION north_face
  FLOW_CONDITION north_face
END

BOUNDARY_CONDITION south_face
  FLOW_CONDITION south_face
  REGION south_face
END
"""
  
  
f.write(simulation)
f.write("SUBSURFACE\n")
f.write(grid)
f.write(region)
f.write(mat_prop)
f.write(char_curves)
f.write(eoswater)
f.write(fluid_prop)
f.write(strata)
f.write(time)
f.write(output)
f.write(dataset)
f.write(flow_cond)
f.write(init_cond)
f.write("\nEND_SUBSURFACE\n") 
# Must have \n at the end, or HDF5 output will not work