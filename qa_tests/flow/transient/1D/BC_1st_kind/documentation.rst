.. _flow-transient-1D-pressure-BC-1st-kind:

************************************************
1D Transient Flow (Pressure), BCs of 1st Kind 
************************************************
:ref:`flow-transient-1D-pressure-BC-1st-kind-description`

:ref:`flow-general-transient-1D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-th-transient-1D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-richards-transient-1D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-transient-1D-pressure-BC-1st-kind-python`



.. _flow-transient-1D-pressure-BC-1st-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.2.7, pg.32, "A Transient 1D 
Pressure Distribution, Time-Dependent Boundary Conditions of 1st Kind."

The domain is a 20x1x1 meter rectangular column extending along the positive 
and negative x-axis and is made up of 20x1x1 cubic grid cells with dimensions 
1x1x1 meters. The domain material is assigned a porosity :math"`\phi` = 0.25, 
and permeability :math:`k` = 1e-14 m2. The fluid has a viscosity :math:`\mu` = 
1.728 mPa-sec, and a constant compressibility :math:`K` = 1e-8 1/Pa.

The pressure is initially uniform at *p(t=0)* = 0.25 MPa.
The boundary pressures are transient:

.. math::
   p(-L,t) = p_b t + p_{t=0}
   
   p(L,t) = p_b t + p_{t=0}

where L = 10 m and :math:`p_b` = 2 MPa/day. The transient pressure 
distribution is governed by,

.. math:: 
   \phi K {{\partial p} \over {\partial t}} = {k \over \mu} {{\partial^{2} p} \over {\partial x^{2}}}

With the given boundary conditions, the solution is defined by,

.. math::
   \chi = {k \over {\phi \mu K}}

   p(x,t) = p_b t + {{p_b(x^2-L^2)}\over{2\chi}} + {{16 p_b L^2}\over{\chi\pi^3}} \sum_{n=0}^{\infty}{{(-1)^n}\over{(2n+1)^3}} cos{\left({{(2n+1)x\pi}\over{2L}}\right)} e^{\left({-\chi(2n+1)^2\pi^2{{t}\over{4L^2}}}\right)}

.. figure:: ../qa_tests/flow/transient/1D/BC_1st_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/flow/transient/1D/BC_1st_kind/general_mode/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/flow/transient/1D/BC_1st_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
      
.. figure:: ../qa_tests/flow/transient/1D/BC_1st_kind/richards_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for RICHARDS mode.
   
   
   
.. _flow-general-transient-1D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The GENERAL Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/transient/1D/BC_1st_kind/general_mode/1D_transient_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_1st_kind/general_mode/1D_transient_pressure_BC_1st_kind.in



.. _flow-th-transient-1D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/transient/1D/BC_1st_kind/th_mode/1D_transient_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_1st_kind/th_mode/1D_transient_pressure_BC_1st_kind.in



.. _flow-richards-transient-1D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (RICHARDS Mode)
=======================================
The RICHARDS Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/transient/1D/BC_1st_kind/richards_mode/1D_transient_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_1st_kind/richards_mode/1D_transient_pressure_BC_1st_kind.in


  
.. _flow-transient-1D-pressure-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: flow_transient_1D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
