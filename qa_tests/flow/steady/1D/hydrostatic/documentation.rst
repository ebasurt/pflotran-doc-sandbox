.. _flow-steady-1D-pressure-hydrostatic:

**************************************
1D Steady Flow (Pressure), Hydrostatic
**************************************
:ref:`flow-steady-1D-pressure-hydrostatic-description`

:ref:`flow-general-steady-1D-pressure-hydrostatic-pflotran-input`

:ref:`flow-th-steady-1D-pressure-hydrostatic-pflotran-input`

:ref:`flow-richards-steady-1D-pressure-hydrostatic-pflotran-input`

:ref:`flow-steady-1D-pressure-hydrostatic-python`



.. _flow-steady-1D-pressure-hydrostatic-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.2.6, pg.32, "A Hydrostatic Pressure 
Distribution."

The domain is a 100x20x20 meter rectangular beam extending along the positive 
z-axis and is made up of 1x1x100 hexahedral grid cells with dimensions 20x20x1 
meters. The material properties should not influence the solution for this test
problem, except for the fluid density. The following properties are assigned: 
porosity = 0.5; 
fluid viscosity :math:`\mu` = 1.0 mPa-s; permeability = 1e-12 m^2; 
fluid density *rho* = 1,019 kg/m^3.

The pressure is initially uniform at *p(t=0)* = 0.5 MPa.
The top pressure boundary condition is *p(z=H)* = 0 MPa and the bottom
of the column is assigned a no flow boundary condition, *q(z=0)* = 0 m/s, 
where H = 100 m.
The simulation is run until the steady-state pressure distribution
develops. 

The solution is given by the hydrostatic pressure distribution, and does not
depend on the material properties assigned,

.. math:: p(z) = \rho g (H - z)

where g is the standard value for gravity (9.81 m/s2).

.. figure:: ../qa_tests/flow/steady/1D/hydrostatic/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/flow/steady/1D/hydrostatic/general_mode/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/flow/steady/1D/hydrostatic/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
.. figure:: ../qa_tests/flow/steady/1D/hydrostatic/richards_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for RICHARDS mode.
   
   
   
.. _flow-general-steady-1D-pressure-hydrostatic-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/1D/hydrostatic/general_mode/1D_steady_pressure_hydrostatic.in>`.

.. literalinclude:: ../qa_tests/flow/steady/1D/hydrostatic/general_mode/1D_steady_pressure_hydrostatic.in



.. _flow-th-steady-1D-pressure-hydrostatic-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/1D/hydrostatic/th_mode/1D_steady_pressure_hydrostatic.in>`.

.. literalinclude:: ../qa_tests/flow/steady/1D/hydrostatic/th_mode/1D_steady_pressure_hydrostatic.in



.. _flow-richards-steady-1D-pressure-hydrostatic-pflotran-input:

The PFLOTRAN Input File (RICHARDS Mode)
=======================================
The RICHARDS Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/1D/hydrostatic/richards_mode/1D_steady_pressure_hydrostatic.in>`.

.. literalinclude:: ../qa_tests/flow/steady/1D/hydrostatic/richards_mode/1D_steady_pressure_hydrostatic.in


  
.. _flow-steady-1D-pressure-hydrostatic-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: flow_steady_1D_hydrostatic
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
