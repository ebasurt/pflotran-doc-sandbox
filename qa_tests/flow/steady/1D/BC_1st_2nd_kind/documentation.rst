.. _flow-steady-1D-pressure-BC-1st-2nd-kind:

************************************************
1D Steady Flow (Pressure), BCs of 1st & 2nd Kind
************************************************
:ref:`flow-steady-1D-pressure-BC-1st-2nd-kind-description`

:ref:`flow-general-steady-1D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`flow-th-steady-1D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`flow-richards-steady-1D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`flow-steady-1D-pressure-BC-1st-2nd-kind-python`

.. _flow-steady-1D-pressure-BC-1st-2nd-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.2.2, pg.27, "A 1D Steady-State 
Pressure Distribution, Boundary Conditions of 1st and 2nd Kind."

The domain is a 100x10x10 meter rectangular beam extending along the positive 
x-axis and is made up of 20x1x1 cubic grid cells with dimensions 5x10x10 
meters. The domain is composed of two materials and is assigned the following 
properties: permeability *k1(x<2L/5)* = 1e-12 m^2; permeability *k2(x>2L/5)* = 
3e-12 m^2; specific heat capacity *Cp* = 0.001 J/(m-C); rock density = 
2,800 kg/m^3; fluid density = 1,000 kg/m^3; fluid viscosity :math:`\mu` = 
1 mPa-s; porosity = 0.50.

The temperature is initially uniform at *T(t=0)* = 25C, and the pressure is
initially uniform at *p(t=0)* = 1.0 MPa.
The temperature at both ends is *T(x=0,x=L)* = 25.0C. The pressure at the left
end is *p(x=0)* = 1.0 MPa, and a fluid flux of *q(x=L)* = -1.5e-5 m/s is applied 
at the right end, where L = 100 m.
The simulation is run until the steady-state pressure distribution
develops. 

The LaPlace equation governs the steady-state pressure distribution,

.. math:: {{\partial^{2} p} \over {\partial x^{2}}} = 0

The solution is given by,

.. math:: 

   p(x<2L/5) = a_1 x + b_1
   
   p(x>2L/5) = a_2 x + b_2

When the boundary conditions *p(x=0)* = 1.0 MPa and *q(x=L)* = -1.5e-5 m/s are 
applied, the solution becomes,

.. math:: 

   p(x<2L/5) = -{{q \mu x} \over {k1}} + p(x=0)
   
   p(x>2L/5) = -{{q \mu x} \over {k2}} + p(x=0) + {{2 q \mu L} \over {5}}\left({1 \over k2}-{1 \over k1}\right)

.. figure:: ../qa_tests/flow/steady/1D/BC_1st_2nd_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/flow/steady/1D/BC_1st_2nd_kind/general_mode/comparison_plot.png
   :width: 49 %   
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/flow/steady/1D/BC_1st_2nd_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
.. figure:: ../qa_tests/flow/steady/1D/BC_1st_2nd_kind/richards_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for RICHARDS mode.


   
.. _flow-general-steady-1D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/1D/BC_1st_2nd_kind/general_mode/1D_steady_pressure_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/1D/BC_1st_2nd_kind/general_mode/1D_steady_pressure_BC_1st_2nd_kind.in



.. _flow-th-steady-1D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/1D/BC_1st_2nd_kind/th_mode/1D_steady_pressure_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/1D/BC_1st_2nd_kind/th_mode/1D_steady_pressure_BC_1st_2nd_kind.in



.. _flow-richards-steady-1D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (RICHARDS Mode)
=======================================
The RICHARDS Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/1D/BC_1st_2nd_kind/richards_mode/1D_steady_pressure_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/1D/BC_1st_2nd_kind/richards_mode/1D_steady_pressure_BC_1st_2nd_kind.in


  
.. _flow-steady-1D-pressure-BC-1st-2nd-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: flow_steady_1D_BC1st2ndkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
