.. _flow-steady-2D-pressure-BC-1st-kind:

******************************************
2D Steady Flow (Pressure), BCs of 1st Kind 
******************************************
:ref:`flow-steady-2D-pressure-BC-1st-kind-description`

:ref:`flow-general-steady-2D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-th-steady-2D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-richards-steady-2D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-steady-2D-pressure-BC-1st-kind-dataset`

:ref:`flow-steady-2D-pressure-BC-1st-kind-python`



.. _flow-steady-2D-pressure-BC-1st-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.2.3, pg.29, "A 2D Steady-State 
Pressure Distribution, Boundary Conditions of 1st Kind."

The domain is a 1x1x1 meter slab extending along the positive 
x-axis and y-axis and is made up of 20x20x1 hexahedral grid cells with 
dimensions 0.05x0.05x1 meters. The materials are assigned the following 
properties: porosity = 0.5; permeability = 1e-15 m2; fluid density *rho* = 1000
kg/m3; fluid viscosity :math:`\mu` = 1 mPa-s.

The pressure is initially uniform at *p(t=0)* = 1.0 MPa.
The boundary pressure conditions (in units of MPa) are:

.. math::
  p(0,y) = 1                      \hspace{0.25in} x=0 \hspace{0.15in} face
  
  p(L,y) = p_0 {y \over L} + 1    \hspace{0.25in} x=L \hspace{0.15in} face
  
  p(x,0) = 1                      \hspace{0.25in} y=0 \hspace{0.15in} face
  
  p(x,L) = p_0 {x \over L} + 1    \hspace{0.25in} y=L \hspace{0.15in} face

where L = 1 m and p\ :sub:`0` = 2.0 MPa.
The simulation is run until the steady-state pressure distribution
develops. 

The LaPlace equation governs the steady-state pressure distribution,

.. math:: {{\partial^{2} p} \over {\partial x^{2}}} + {{\partial^{2} p} \over {\partial y^{2}}}= 0

After applying these boundary conditions, the solution is given by,

.. math:: p(x,y) = {x \over L}{y \over L} + p_0

.. figure:: ../qa_tests/flow/steady/2D/BC_1st_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/flow/steady/2D/BC_1st_kind/general_mode/comparison_plot.png
   :width: 49 %   
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/flow/steady/2D/BC_1st_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
.. figure:: ../qa_tests/flow/steady/2D/BC_1st_kind/richards_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for RICHARDS mode.
   
   
   
.. _flow-general-steady-2D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/2D/BC_1st_kind/general_mode/2D_steady_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/2D/BC_1st_kind/general_mode/2D_steady_pressure_BC_1st_kind.in



.. _flow-th-steady-2D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/2D/BC_1st_kind/th_mode/2D_steady_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/2D/BC_1st_kind/th_mode/2D_steady_pressure_BC_1st_kind.in



.. _flow-richards-steady-2D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (RICHARDS Mode)
=======================================
The Richards Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/2D/BC_1st_kind/richards_mode/2D_steady_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/2D/BC_1st_kind/richards_mode/2D_steady_pressure_BC_1st_kind.in


  
.. _flow-steady-2D-pressure-BC-1st-kind-dataset:

The Dataset
===========
The hdf5 dataset required to define the initial/boundary conditions is created
with the following python script called ``create_dataset.py``:

.. literalinclude:: ../qa_tests/flow/steady/2D/BC_1st_kind/create_dataset.py
  
  
  
.. _flow-steady-2D-pressure-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: flow_steady_2D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
