.. _flow-steady-3D-pressure-BC-1st-kind:

******************************************
3D Steady Flow (Pressure), BCs of 1st Kind 
******************************************
:ref:`flow-steady-3D-pressure-BC-1st-kind-description`

:ref:`flow-general-steady-3D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-th-steady-3D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-richards-steady-3D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-steady-3D-pressure-BC-1st-kind-dataset`

:ref:`flow-steady-3D-pressure-BC-1st-kind-python`



.. _flow-steady-3D-pressure-BC-1st-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.2.5, pg.31, "A 3D Steady-State 
Pressure Distribution, Boundary Conditions of 1st Kind."

The domain is a 1x1x1 meter cube extending along the positive 
x-axis, y-axis, and z-axis and is made up of 10x10x10 cubic grid cells with 
dimensions 0.1x0.1x0.1 meters. The domain material is assigned the following 
properties:  permeability *k* = 1e-10 m^2; rock density = 
2,800 kg/m^3; fluid density = 1,000 kg/m^3; fluid viscosity :math:`\mu` = 
1 mPa-s; porosity = 0.50.

The pressure is initially uniform at *p(t=0)* = 1.0 MPa.
The pressure boundary conditions (in units of MPa) are:

.. math::
  p(0,y,z) = p_0 \left( {{0}+{y \over L}+{z \over L}} \right) \hspace{0.25in} x=0 \hspace{0.15in} face
  
  
  p(x,0,z) = p_0 \left( {{x \over L}+{0}+{z \over L}} \right) \hspace{0.25in} y=0 \hspace{0.15in} face
  
  
  p(x,y,0) = p_0 \left( {{x \over L}+{y \over L}+{0}} \right) \hspace{0.25in} z=0 \hspace{0.15in} face
  
  
  p(L,y,z) = p_0 \left( {{L}+{y \over L}+{z \over L}} \right) \hspace{0.25in} x=L \hspace{0.15in} face
  
  
  p(x,L,z) = p_0 \left( {{x \over L}+{L}+{z \over L}} \right) \hspace{0.25in} y=L \hspace{0.15in} face
  
  
  p(x,y,L) = p_0 \left( {{x \over L}+{y \over L}+{L}} \right) \hspace{0.25in} z=L \hspace{0.15in} face

where L = 1 m and p\ :sub:`0` = 1 MPa. The simulation is run until the 
steady-state pressure distribution develops. 

The LaPlace equation governs the steady-state pressure distribution,

.. math:: {{\partial^{2} p} \over {\partial x^{2}}} + {{\partial^{2} p} \over {\partial y^{2}}} + {{\partial^{2} p} \over {\partial z^{2}}} = 0

The solution is given by,

.. math:: p(x,y,z) = p_{0} \left( {x \over L}+{y \over L}+{z \over L} \right)

.. figure:: ../qa_tests/flow/steady/3D/BC_1st_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/flow/steady/3D/BC_1st_kind/general_mode/comparison_plot.png
   :width: 49 %   
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/flow/steady/3D/BC_1st_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
.. figure:: ../qa_tests/flow/steady/3D/BC_1st_kind/richards_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for RICHARDS mode.
   
   
   
.. _flow-general-steady-3D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The GENERAL Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/3D/BC_1st_kind/general_mode/3D_steady_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/3D/BC_1st_kind/general_mode/3D_steady_pressure_BC_1st_kind.in



.. _flow-th-steady-3D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/3D/BC_1st_kind/th_mode/3D_steady_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/3D/BC_1st_kind/th_mode/3D_steady_pressure_BC_1st_kind.in



.. _flow-richards-steady-3D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (RICHARDS Mode)
=======================================
The RICHARDS Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/3D/BC_1st_kind/richards_mode/3D_steady_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/3D/BC_1st_kind/richards_mode/3D_steady_pressure_BC_1st_kind.in


  
.. _flow-steady-3D-pressure-BC-1st-kind-dataset:

The Dataset
===========
The hdf5 dataset required to define the initial/boundary conditions is created
with the following python script called ``create_dataset.py``:

.. literalinclude:: ../qa_tests/flow/steady/3D/BC_1st_kind/create_dataset.py
  
  
  
.. _flow-steady-3D-pressure-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: flow_steady_3D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
