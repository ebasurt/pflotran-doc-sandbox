import sys
from h5py import *
import numpy as np
import math
import matplotlib.pyplot as plt

filename = 'dataset.h5'
h5file = File(filename,mode='w')

T_offset = 0.10  # [C]
T0 = 1.          # [C]
L = 100.         # [m]
dx = 1.0         # [m]
dy = 1.0         # [m]
dz = 1.0         # [m]

# 2D Surface:
# -----------------------------------------------------------------------------
# Temperature initial condition z=0 face
h5grp = h5file.create_group('initial')
h5grp.attrs['Dimension'] = np.string_('XY')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [dx,dy]
# Location of origin
h5grp.attrs['Origin'] = [0.,0.]
# Load the dataset values
nx = L*dx + 1
ny = L*dy + 1
rarray = np.zeros((nx,ny),'=f8')

fx = np.zeros(nx,'=f8')
for i in range(int(nx)):
  if (0. <= i < (L/10.)):
    fx[i] = 0.
  if ((L/10.) <= i < (4.*L/10.)):
    fx[i] = (10./(3.*L))*float(i) - (1./3.)
  if ((4.*L/10.) <= i < (6.*L/10.)):
    fx[i] = 1.
  if ((6.*L/10.) <= i < (9.*L/10.)):
    fx[i] = 3. - (10./(3.*L))*float(i)
  if ((9.*L/10.) <= i < L):
    fx[i] = 0.
    
fy = np.zeros(ny,'=f8')
for j in range(int(ny)):
  if (0. <= j < (L/10.)):
    fy[j] = 0.
  if ((L/10.) <= j < (4.*L/10.)):
    fy[j] = (10./(3.*L))*float(j) - (1./3.)
  if ((4.*L/10.) <= j < (6.*L/10.)):
    fy[j] = 1.
  if ((6.*L/10.) <= j < (9.*L/10.)):
    fy[j] = 3. - (10./(3.*L))*float(j)
  if ((9.*L/10.) <= j < L):
    fy[j] = 0.

for i in range(int(nx)):
  for j in range(int(ny)):
    rarray[i][j] = T0*fx[i]*fy[j] + T_offset
    
h5dset = h5grp.create_dataset('Data', data=rarray)




