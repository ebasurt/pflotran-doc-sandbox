.. _gas-steady-1D-pressure-BC-1st-2nd-kind:

***********************************************
1D Steady Gas (Pressure), BCs of 1st & 2nd Kind
***********************************************
:ref:`gas-steady-1D-pressure-BC-1st-2nd-kind-description`

:ref:`gas-general-steady-1D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`gas-steady-1D-pressure-BC-1st-2nd-kind-python`


.. _gas-steady-1D-pressure-BC-1st-2nd-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.3.2, pg.41, "A 1D Steady-State 
Gas Pressure Distribution, Boundary Conditions of 1st and 2nd Kind."

The domain is a 40x5x5 meter rectangular beam extending along the positive 
x-axis and is made up of 40x1x1 cubic grid cells with dimensions 1x5x5 
meters. The materials are assigned the following properties: porosity = 0.5; 
gas viscosity :math:`\mu` = 1.0e-5 Pa-s; permeability = 1e-15 m^2; 
gas saturation :math:`S_g` = 1.0 in the entire domain.

The gas pressure is initially uniform at *p(t=0)* = 1.0e5 Pa.
At the east boundary, a dirichlet gas pressure *p(x=L)* = 1.0e5 Pa is applied, 
where L = 40 m. At the west boundary, a specific gas flow is applied
*Q(x=0)* = 0.17 Pa m/s,

.. math::

   Q = -\frac{k}{\mu}p\frac{dp}{dx}

PFLOTRAN does not have the capability to handle such a boundary condition.
To convert specific gas flow to a gas flux in m/s, the ideal gas law is used 
by dividing the specific gas flow by the boundary pressure, 

.. math::

  p = \frac{n}{V}RT
  
where :math:`\frac{n}{V}` is the gas density at the boundary in mol m\ 
:sup:`-1`, R is the ideal gas constant (8.314), and T is the temperature in 
Kelvin. Therefore, the specific gas flow boundary at x=0 is converted to a
gas flux boundary as,

.. math::
   :label: gas_flux
   
   q = \frac{0.17}{\frac{n}{V}RT}

As gas flows into the domain, pressure increases, and as a result gas
density also increases. To figure out what value of gas flux must be applied
at the west boundary, we can use the analytical solution for gas pressure at 
x=0. Assuming a certain temperature, the gas density can be calculated using
the ideal gas law. That gas density can then be used in equation 
:eq:`gas_flux` to set the value of the gas flux boundary condition. In this
example, the gas pressure at steady state at the west boundary is approximately
4.4358 kg m\ :sup:`-3`, which is converted to approximately 153.17 mol m\ 
:sup:`-3`, giving :math:`\frac{n}{V}`. The values for R and T are known.

The simulation is run until the steady-state pressure distribution
develops, which is 20 years. 

The LaPlace equation governs the steady-state gas pressure distribution,

.. math:: {{\partial^{2} p^{2}} \over {\partial x^{2}}} = 0

The solution is given by,

.. math:: p(x) = \sqrt{ax + b}

When the boundary conditions are applied, the solution becomes,

.. math:: p(x) = \sqrt{\frac{2 Q \mu}{k}(L-x) + p_1^2}

.. figure:: ../qa_tests/gas/steady/1D/BC_1st_2nd_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/gas/steady/1D/BC_1st_2nd_kind/general_mode/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
   
   
.. _gas-general-steady-1D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The GENERAL Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/gas/steady/1D/BC_1st_2nd_kind/general_mode/1D_steady_gas_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/gas/steady/1D/BC_1st_2nd_kind/general_mode/1D_steady_gas_BC_1st_2nd_kind.in


  
.. _gas-steady-1D-pressure-BC-1st-2nd-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: gas_steady_1D_BC1st2ndkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
